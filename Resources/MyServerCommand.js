﻿var MyServerCommand = (function (_super) {
    __extends(MyServerCommand, _super);
    function MyServerCommand() {
        return _super !== null && _super.apply(this, arguments) || this;
    }

    MyServerCommand.prototype.execute = function () {
        // Get settings
        var commandSettings = this.CommandParam;
        var normalProperty = commandSettings.NormalProperty;
        var formulaProperty = commandSettings.FormulaProperty;
        var formulaValue = this.evaluateFormula(formulaProperty);

        // Add command logic here
        alert("execute command: " + normalProperty + " " + formulaValue);
    };

    return MyServerCommand;
}(Forguncy.CommandBase));

// Key format is "Namespace.ClassName, AssemblyName"
Forguncy.CommandFactory.registerCommand("MyServerCommand.MyServerCommand, MyServerCommand", MyServerCommand);
﻿using GrapeCity.Forguncy.Commands;
using GrapeCity.Forguncy.Plugin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyServerCommand
{
    [Icon("pack://application:,,,/MyServerCommand;component/Resources/Icon.png")]
    public class MyServerCommand : Command, ICommandExecutableInServerSide
    {
        public ExecuteResult Execute(IServerCommandExecuteContext dataContext)
        {
            //Do some thing
            return new ExecuteResult();
        }
    }
}
